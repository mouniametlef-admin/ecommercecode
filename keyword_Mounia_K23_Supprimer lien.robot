keyword_Mounia_K23_Supprimer lien 
    # vSite contient le nom du site
    # vlien contient le nom du lien
    [Arguments]    ${vSite}    ${vLien}
    ### Ouvrir le site ###
    #cliquer sur le lien Site         
    Click Element    ${link_HeaderSites} 
    #attendre que le lien Recherche de Sites soit visible
    Wait Until Element Is Visible    ${link_HeaderRechercheDeSites}
    #cliquer sur le lien Recherche de Sites
    Click Element    ${link_HeaderRechercheDeSites}
    sleep    2
    #saisir le nom de site dans la case de recherche
    Input Text    ${txt_SiteSearch}    ${vSite} 
    sleep    2 
    #cliquer sur le boutton Rechercher
    Click Button    ${btn_rechercher} 
    sleep    2   
    #attendre que le nom de site soit visible 
    Wait Until Element Is Visible    ${link_site_part1}${vSite}${link_site_part2}
    #cliquer sur le nom du site   
    Click Element    ${link_site_part1}${vSite}${link_site_part2} 
    Sleep    2
    ### Ouvrir page Lien ###
    #cliquer sur le lien Liens s'il est sur la page du site
    ${statut}    ${valeur}=    Run Keyword And Ignore Error    Click Link  ${link_Liens} 
    #sinon cliquer sur le lien Plus
    Run Keyword if    '${statut}'=='FAIL'    Click Element    ${link_Plus}
    sleep    1
    #cliquer sur le lien Liens
    Run Keyword if    '${statut}'=='FAIL'    Click Element    ${link_Liens}  
    Sleep    2
    ### supprimer lien ###
    #cocher la case du lien à supprimer
    Select Checkbox     ${chk_Lien_part1}${vLien}${chk_Lien_part2}
    Sleep    2
    #cliquer sur le lien Supprimer
    Click Element    ${link_Supprimer_Lien_part1}${vLien}${link_Supprimer_Lien_part2}
    Sleep    5
    #confirmer la suppression du lien
    Click Element   ${btn_LienSupprimer_Supprimer}
    ####################FinKeyword###########	